package com.verizon.vdc.service;

import com.verizon.vdc.model.Application;
import com.verizon.vdc.model.enums.StageType;
import com.verizon.vdc.model.enums.StepStatusType;
import com.verizon.vdc.model.enums.StepType;
import com.verizon.vdc.model.minors.CreationStatus;
import com.verizon.vdc.model.minors.PlatformSettings;
import com.verizon.vdc.model.minors.Status;
import com.verizon.vdc.repository.ApplicationRepository;
import com.verizon.vdc.util.AppProperties;
import com.verizon.vdc.util.BashExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.CompletableFuture;

@Service
public class PipelineService {

    private AppProperties appProperties;
    private BashExecutor bashExecutor;
    private ApplicationRepository applicationRepository;

    @Autowired
    public PipelineService(AppProperties appProperties, BashExecutor bashExecutor, ApplicationRepository applicationRepository) {
        this.appProperties = appProperties;
        this.bashExecutor = bashExecutor;
        this.applicationRepository = applicationRepository;
    }

    private static Logger LOGGER = LoggerFactory.getLogger(PipelineService.class);

    public boolean createJenkinsFile(Application application) {

        String pipelineCode = this.createPipelineString(application);
        if (pipelineCode == null) {
            LOGGER.error("The pipelice code was not created for " + application.toString());
            return false;
        }

        try {
            try (PrintWriter printWriter = new PrintWriter(appProperties.getPipelinePath().concat("/" + application.getId().concat(".pipeline")))) {
                printWriter.write(pipelineCode);
                return true;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    public String createOnboardPipeline(Application application) {
        String pipelineCode = this.createPipelineString(application);
        pipelineCode = pipelineCode.replace("GIT_URL", application.getPlatformSettings().getOnestashUrl());
        return pipelineCode;
    }

    /*****************************************************************/

    private String createPipelineString(Application application) {
        String pipeline = application.getPipeline();
        StringBuilder jenkinsFileBuilder = new StringBuilder();

        jenkinsFileBuilder.append("node {\n");

        // ********* DECLARATIONS
        if (pipeline.contains(StepType.MAVEN.name())) {
            jenkinsFileBuilder.append("\t def mvnHome \n");
        }

        // ********* PREPARATION
        jenkinsFileBuilder.append("\t stage('PREPARATION') {\n");
        jenkinsFileBuilder.append("\t\t "+createPipeline(application.getId())+"\n");
        // if maven is defined
        if (pipeline.contains(StepType.MAVEN.name())) {
            jenkinsFileBuilder.append("\t\t mvnHome = tool 'M3'\n");
        }
        jenkinsFileBuilder.append("\t }\n");

        // ********* SCM
        if (pipeline.contains(StepType.GIT.name())) {
            jenkinsFileBuilder.append("\t stage('" + StageType.SCM.name()+ "') {\n");
            jenkinsFileBuilder.append("\t\t ".concat(updatePipeline(StageType.SCM.name(), "0", "STARTED", application.getId())).concat("\n"));
            jenkinsFileBuilder.append("\t\t git 'GIT_URL'\n");
            jenkinsFileBuilder.append("\t\t ".concat(updatePipeline(StageType.SCM.name(), "100", "DONE", application.getId())).concat("\n"));
            jenkinsFileBuilder.append("\t } \n");
        }

        // ********* BUILD
        if (pipeline.contains(StepType.MAVEN.name()) || pipeline.contains(StepType.GRADLE.name())) {
            jenkinsFileBuilder.append("\t stage('" + StageType.BUILD.name() + "') {\n");
            if (pipeline.contains("MAVEN")) {
                jenkinsFileBuilder.append("\t\t ".concat(updatePipeline(StageType.BUILD.name(), "0", "STARTED", application.getId())).concat("\n"));
//                jenkinsFileBuilder.append("\t" + getMavenBuildCode() + "\n");
                jenkinsFileBuilder.append(WrapCommand(StageType.BUILD.name(), getMavenBuildCode(), application.getId()));
                jenkinsFileBuilder.append("\t\t ".concat(updatePipeline(StageType.BUILD.name(), "100", "DONE", application.getId())).concat("\n"));
            }
            jenkinsFileBuilder.append("\t } \n");
        }

        // ********* DEPLOY
        if (pipeline.contains(StepType.CLOUD_FOUNDRY.name())) {

            jenkinsFileBuilder.append("\t stage('"+ StageType.DEPLOY.name() +"') {\n");
            jenkinsFileBuilder.append(WrapCommand(StageType.DEPLOY.name(), getCloudFoundryDeployCode(application), application.getId()));
            jenkinsFileBuilder.append("\t }\n");
        }

        jenkinsFileBuilder.append("}\n");
        return jenkinsFileBuilder.toString();
    }

    private String getCloudFoundryDeployCode(Application application) {
        String cfLoginCmd = String.format("cf login -a %s -u %s -p %s -o %s -s %s", appProperties.getDefaultCfHost(), appProperties.getDefaultCfUser(), appProperties.getDefaultCfPassword(), appProperties.getDefaultCfOrg(), appProperties.getDefaultCfSpace());
        StringBuilder sb = new StringBuilder();
        sb.append("\t\t ".concat(updatePipeline(StageType.DEPLOY.name(), "0", "STARTED", application.getId())).concat("\n"));
        sb.append(String.format("\t\t\tsh \"%s\"\n", cfLoginCmd));
        sb.append("\t\t\t ".concat(updatePipeline(StageType.DEPLOY.name(), "50", "RUNNING", application.getId())).concat("\n"));
        sb.append("\t\t\tsh \"cf push\"\n");
        sb.append("\t\t\t ".concat(updatePipeline(StageType.DEPLOY.name(), "100", "DONE", application.getId())).concat("\n"));
        return sb.toString();
    }

    private String getMavenBuildCode() {
        return "\tif (isUnix()) {\n" +
                "\t        sh \"'${mvnHome}/bin/mvn' -Dmaven.test.failure.ignore clean package\"\n" +
                "\t    } else {\n" +
                "\t         bat(/\"${mvnHome}\\bin\\mvn\" -Dmaven.test.failure.ignore clean package/)\n" +
                "\t    }";
    }

    private String updatePipeline(String stage, String percentage, String status, String appId) {
        return "sh \"curl -X PUT -H 'Content-Type:application/json' -d '{\\\"stage\\\": \\\""+stage+"\\\", \\\"status\\\": {\\\"percentage\\\": \\\""+percentage+"\\\", \\\"status\\\":\\\""+status+"\\\"}}' " +
                "http://localhost:9000/api/execution/"+appId+"\"";
    }

    private String createPipeline(String appId) {
        return "sh \"curl -X POST -H 'Content-Type:application/json' -d '{\\\"appId\\\": \\\""+appId
                +"\\\"}' http://localhost:9000/api/execution\"";
    }

    /******************************************************************/

    private String WrapCommand(String stage, String cmd, String appId) {
        StringBuilder sb = new StringBuilder();
        sb.append("\t\ttry {\n");
        sb.append("\t\t\t" + cmd + "\n");
        sb.append("\t\t} catch(e) {\n");
        sb.append("\t\t\t" + updatePipeline(stage, "0", "FAILED", appId ) + "\n");
        sb.append("\t\t\tthrow e\n");
        sb.append("\t}\n");
        return sb.toString();
    }

    @Async
    public CompletableFuture<Status> startPipeline(String appId) throws IOException {
        LOGGER.info("Run pipeline for app: " + appId);

        Application application = applicationRepository.findOne(appId);
        if (application == null) {
            LOGGER.info("Not aplication was found for App with " + appId);
            return null;
        }
        PlatformSettings platformSettings = application.getPlatformSettings();
        String jkUrl = platformSettings.getJenkinsUrl().concat("/job/");
        String job = appProperties.getPipelineTemplateName().replace("REPLACE", platformSettings.getArtifactId());
        String user = platformSettings.getJenkinsCredentials().getUsername();
        String pass = platformSettings.getJenkinsCredentials().getPassword();

        bashExecutor.execute(startCommand(jkUrl, job, user, pass));
        return CompletableFuture.completedFuture(new Status(100, StepStatusType.DONE));
    }

    private String[] startCommand(String uri, String job, String user, String pass) {
        String[] parammeters = {
                "sh",
                appProperties.getStartPipelineScript(),
                uri,
                job,
                user,
                pass
        };
        return parammeters;
    }

}
