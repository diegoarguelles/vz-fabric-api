package com.verizon.vdc.service;

import com.verizon.vdc.model.Application;
import com.verizon.vdc.model.enums.ProjectType;
import com.verizon.vdc.model.enums.StepStatusType;
import com.verizon.vdc.model.minors.Link;
import com.verizon.vdc.model.minors.PlatformSettings;
import com.verizon.vdc.model.minors.Status;
import com.verizon.vdc.repository.ApplicationRepository;
import com.verizon.vdc.util.AppProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class ApplicationService {

    private static Logger LOGGER = LoggerFactory.getLogger(ApplicationService.class);

    private ApplicationRepository applicationRepository;
    private PipelineService pipelineService;
    private AppProperties appProperties;

    @Autowired
    public ApplicationService(ApplicationRepository applicationRepository, PipelineService pipelineService, AppProperties appProperties) {
        this.applicationRepository = applicationRepository;
        this.pipelineService = pipelineService;
        this.appProperties = appProperties;
    }

    public Application createApp(Application application) {
        application.setFabricStatus(new Status(0, StepStatusType.RUNNING));
        application.setCreatedAt(System.currentTimeMillis());

        // Prepare links
        PlatformSettings settings = application.getPlatformSettings();
        String jenkinsUrl = settings.getJenkinsUrl()
                .concat("/job/").concat(appProperties.getPipelineTemplateName().replace("REPLACE", settings.getArtifactId()));

        String stashUrl = settings.getOnestashUrl();
        if (application.getProjectType() == ProjectType.FABRIC) {
            stashUrl = settings.getOnestashUrl()
                    .concat("/scm/").concat(appProperties.getDefaultStashProject()) // project
                    .concat("/").concat(settings.getArtifactId())
                    .concat(".git");
        }

        String cfUrl = appProperties.getDefaultCfDeploy().replace("REPLACE", settings.getArtifactId()).concat("/api/hello");

        List<Link> links = new ArrayList<>();
        links.add(new Link("repository", stashUrl));
        links.add(new Link("pipeline", jenkinsUrl));
        links.add(new Link("deploy", cfUrl));

        application.setDeploymentLinks(links);
        return applicationRepository.save(application);
    }

    public Application updateApplicationStatus(String appId, Status status) {

        Application application = applicationRepository.findOne(appId);

        if (application == null) {
            LOGGER.error(String.format("No application was found for appId: %s", appId));
            return null;
        }

        if (status.getStatus() == StepStatusType.DONE) {

            try {
                pipelineService.startPipeline(appId);
            } catch (IOException e) {
                LOGGER.error(String.format("Something went wrong when executing the pipeline for appId: %s", appId));
                e.printStackTrace();
               return null;
            }
        }

        application.setFabricStatus(status);
        return applicationRepository.save(application);
    }

    public Application findApplicationById(String appId){
        return applicationRepository.findOne(appId);
    }

    public List<Application> findAll(){
        return applicationRepository.findAll();
    }

}
