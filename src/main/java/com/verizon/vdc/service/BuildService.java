package com.verizon.vdc.service;

import com.verizon.vdc.App;
import com.verizon.vdc.model.Application;
import com.verizon.vdc.model.minors.CreationStatus;
import com.verizon.vdc.util.BashExecutor;
import com.verizon.vdc.util.AppProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.CompletableFuture;

@Service
public class BuildService {

    private static Logger LOGGER = LoggerFactory.getLogger(BuildService.class);

    private BashExecutor bashExecutor;
    private AppProperties appProperties;
    private PipelineService pipelineService;

    @Autowired
    public BuildService(BashExecutor bashExecutor, AppProperties appProperties, PipelineService pipelineService) {
        this.bashExecutor = bashExecutor;
        this.appProperties = appProperties;
        this.pipelineService = pipelineService;
    }

    @Async
    public CompletableFuture<CreationStatus> build(Application application) throws IOException {
        LOGGER.info("BuildService.build " + application.toString());
        // create jenkins file
        pipelineService.createJenkinsFile(application);

        // call executor.sh
        bashExecutor.execute(buildCommand(application));
        CreationStatus creation = new CreationStatus("0", "In Progress");
        return CompletableFuture.completedFuture(creation);
    }

    //    @Async
    public CompletableFuture<CreationStatus> onboard(Application application) throws IOException {
        LOGGER.info("BuildService.onboard " + application.toString());

        // create jenkins file
        String onBoardPipeline = pipelineService.createOnboardPipeline(application);
        String jenkinsJobContent = new String(Files.readAllBytes(Paths.get(appProperties.getOnboardPipelineTemplate())));
        jenkinsJobContent = jenkinsJobContent.replace("PIPELINE_AS_CODE", onBoardPipeline)
                                             .replaceAll("VZ_ID", application.getVzId());

        try (PrintWriter printWriter = new PrintWriter(appProperties.getPipelinePath().concat("/" + application.getId().concat(".xml")))) {
            printWriter.write(jenkinsJobContent);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            CreationStatus creation = new CreationStatus("0", "FAILED");
            return CompletableFuture.completedFuture(creation);
        }

        // call executor.sh
        bashExecutor.execute(onboardCommand(appProperties.getOnboardScript(), application));
        CreationStatus creation = new CreationStatus("0", "RUNNING");
        return CompletableFuture.completedFuture(creation);
    }


    private String[] onboardCommand(String exec, Application application) {

        String jkUri = application.getPlatformSettings().getJenkinsUrl();
        String[] parameters = {
               "sh",
                appProperties.getOnboardScript(),
                application.getId(),
                jkUri,
                application.getPlatformSettings().getJenkinsCredentials().getUsername(),
                application.getPlatformSettings().getJenkinsCredentials().getPassword(),
                application.getPlatformSettings().getArtifactId()
        };

        return parameters;
    }

    private String[] buildCommand(Application application) {
        String[] scmUriData = splitURL(application.getPlatformSettings().getOnestashUrl());
        String[] jkUriData = splitURL(application.getPlatformSettings().getJenkinsUrl());
        String[] parameters = {
                "sh",
                appProperties.getBuildScript(),
                application.getId(),
                application.getPlatformSettings().getGroupId(),
                application.getPlatformSettings().getArtifactId(),
                application.getPlatformSettings().getVersion(),
                scmUriData[1],
                application.getPlatformSettings().getOnestashCredentials().getUsername(),
                application.getPlatformSettings().getOnestashCredentials().getPassword(),
                application.getReplaceRepository().toString(),
                application.getVzId(),
                jkUriData[1],
                application.getPlatformSettings().getJenkinsCredentials().getUsername(),
                application.getPlatformSettings().getJenkinsCredentials().getPassword(),
                scmUriData[0],
                jkUriData[0],
                appProperties.getDefaultStashProject()
        };
        return parameters;
    }

    private String[] splitURL(String url) {
        String protocol = null;
        String domain = null;
        if (url.indexOf("https://") != -1) {
            protocol = "https";
            domain = url.replace("https://", "");
        } else if (url.indexOf("http://") != -1) {
            protocol = "http";
            domain = url.replace("http://", "");
        }
        return new String[]{protocol, domain};
    }

}

