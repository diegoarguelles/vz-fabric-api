package com.verizon.vdc.service;

import com.verizon.vdc.model.Application;
import com.verizon.vdc.model.Execution;
import com.verizon.vdc.model.enums.StageType;
import com.verizon.vdc.model.enums.StepStatusType;
import com.verizon.vdc.model.enums.StepType;
import com.verizon.vdc.model.minors.Status;
import com.verizon.vdc.model.minors.Step;
import com.verizon.vdc.repository.ApplicationRepository;
import com.verizon.vdc.repository.ExecutionRepository;
import com.verizon.vdc.rest.payloads.ExecutionUpdatePayload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ExecutionService {

    private static Logger LOGGER = LoggerFactory.getLogger(ExecutionService.class);

    private ExecutionRepository executionRepository;
    private ApplicationRepository applicationRepository;

    private Map<StepType, StageType> stepStageMap;

    @Autowired
    public ExecutionService(ExecutionRepository executionRepository, ApplicationRepository applicationRepository) {
        this.executionRepository = executionRepository;
        this.applicationRepository = applicationRepository;
        this.mapStepWithStages();

    }

    private void mapStepWithStages() {
        this.stepStageMap = new HashMap<>();
        this.stepStageMap.put(StepType.GIT, StageType.SCM);
        this.stepStageMap.put(StepType.MAVEN, StageType.BUILD);
        this.stepStageMap.put(StepType.GRADLE, StageType.BUILD);
        this.stepStageMap.put(StepType.CLOUD_FOUNDRY, StageType.DEPLOY);
    }

    public Execution createExecutionEntry(String appId) {

        Application application = applicationRepository.findOne(appId);
        String pipelineSteps[] = application.getPipeline().split(","); // FIXME: Chagen this for an array of StepType

        List<Step> steps = new ArrayList<>();
        String added = "";
        // PULL, BUILD, DEPLOY

        for(String pipelineStep: pipelineSteps){
            StageType stageType = this.stepStageMap.get(StepType.valueOf(pipelineStep));

            if (added.contains(stageType.name())) continue;

            Step step = new Step();
            step.setName(stageType.name());
            step.setStatus(new Status(0, StepStatusType.PENDING));
            steps.add(step);

            added = added.concat(stageType.name());

        }

        Execution execution = new Execution();
        execution.setAppId(appId);
        execution.setSteps(steps);
        execution.setFinished(false);
        execution.setStartedAt(System.currentTimeMillis());

        return executionRepository.save(execution);

    }

    public Execution updateStatus(String appId, ExecutionUpdatePayload updatePayload) {

        List<Execution> executions = executionRepository.findByAppIdAndFinished(appId, false);

        if (executions.size() != 1) {
            LOGGER.warn("There are more than 1 active executions for the appId: ".concat(appId));
            return null;
        }

        Execution execution = executions.get(0);
        if (execution == null) {
            LOGGER.error("The execution was not found for appId: ".concat(appId));
            return null;
        }

        List<Step> steps = execution.getSteps();
        if (steps.isEmpty()) {
            LOGGER.error("The pipeline steps is empty for appId: ".concat(appId));
            return null;
        }

        int found = steps.indexOf(new Step(updatePayload.getStage().name()));
        if (found == -1) {
            LOGGER.error(String.format("The pipeline step %s was not found for appId: %s", updatePayload.getStage().name(), appId));
            return null;
        }

        Step step = steps.get(found);
        step.setStatus(updatePayload.getStatus());

        StepStatusType stepStatus = updatePayload.getStatus().getStatus();

        switch (stepStatus) {
            case STARTED:
                step.setStartedAt(System.currentTimeMillis()); // JUST THE FIRST Execution
                break;
            case DONE:
                step.setFinishedAt(System.currentTimeMillis());
                break;
        }

        if (updatePayload.getStage() == StageType.DEPLOY && updatePayload.getStatus().getStatus() == StepStatusType.DONE) {
            execution.setFinishedAt(System.currentTimeMillis());
            execution.setFinished(true);
        }

        if(updatePayload.getStatus().getStatus() == StepStatusType.FAILED) {
            execution.setFinished(true);
        }

        return executionRepository.save(execution);
    }

    public List<Execution> getAllExecutionsOnApplication(String appId){
//        return executionRepository.findAllByAppId(appId);
        return executionRepository.findAllByAppIdOrderByStartedAtDesc(appId);
    }
}
