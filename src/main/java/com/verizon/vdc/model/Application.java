package com.verizon.vdc.model;

import com.verizon.vdc.model.enums.ProjectType;
import com.verizon.vdc.model.minors.Link;
import com.verizon.vdc.model.minors.Platform;
import com.verizon.vdc.model.minors.PlatformSettings;
import com.verizon.vdc.model.minors.Status;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.Map;

@Document(collection = "projects")
public class Application {

    @Id
    private String id;
    private String vzId;
    private String name;
    private Integer replaceRepository;
    private Platform platform;
    private PlatformSettings platformSettings;
    private String pipeline;
    private String legacyUrl;
    private Status fabricStatus;
    private Long createdAt;
    private Long finishedAt;
    private ProjectType projectType;
    private List<Link> deploymentLinks;

    public Application() {
    }

    public List<Link> getDeploymentLinks() {
        return deploymentLinks;
    }

    public void setDeploymentLinks(List<Link> deploymentLinks) {
        this.deploymentLinks = deploymentLinks;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVzId() {
        return vzId;
    }

    public void setVzId(String vzId) {
        this.vzId = vzId;
    }

    public Integer getReplaceRepository() {
        return replaceRepository;
    }

    public void setReplaceRepository(Integer replaceRepository) {
        this.replaceRepository = replaceRepository;
    }

    public Platform getPlatform() {
        return platform;
    }

    public void setPlatform(Platform platform) {
        this.platform = platform;
    }

    public PlatformSettings getPlatformSettings() {
        return platformSettings;
    }

    public void setPlatformSettings(PlatformSettings platformSettings) {
        this.platformSettings = platformSettings;
    }

    public String getPipeline() {
        return pipeline;
    }

    public void setPipeline(String pipeline) {
        this.pipeline = pipeline;
    }

    public Status getFabricStatus() {
        return fabricStatus;
    }

    public void setFabricStatus(Status fabricStatus) {
        this.fabricStatus = fabricStatus;
    }

    public String getLegacyUrl() {
        return legacyUrl;
    }

    public void setLegacyUrl(String legacyUrl) {
        this.legacyUrl = legacyUrl;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public ProjectType getProjectType() {
        return projectType;
    }

    public void setProjectType(ProjectType projectType) {
        this.projectType = projectType;
    }

    public Long getFinishedAt() {
        return finishedAt;
    }

    public void setFinishedAt(Long finishedAt) {
        this.finishedAt = finishedAt;
    }

    @Override
    public String toString() {
        return "App{" +
                "id='" + id + '\'' +
                ", vzId='" + vzId + '\'' +
                ", replaceRepository=" + replaceRepository +
                ", platform=" + platform +
                ", platformSettings=" + platformSettings +
                ", pipeline='" + pipeline + '\'' +
                ", legacyUrl='" + legacyUrl + '\'' +
                ", fabricStatus='" + fabricStatus + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", projectType='" + projectType + '\'' +
                '}';
    }
}
