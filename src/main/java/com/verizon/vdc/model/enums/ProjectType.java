package com.verizon.vdc.model.enums;

public enum ProjectType {
    FABRIC,
    LEGACY
}
