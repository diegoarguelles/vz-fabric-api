package com.verizon.vdc.model.enums;

public enum StepStatusType {
    PENDING,
    STARTED,
    RUNNING,
    DONE,
    FAILED
}
