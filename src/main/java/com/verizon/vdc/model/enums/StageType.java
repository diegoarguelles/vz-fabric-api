package com.verizon.vdc.model.enums;

public enum StageType {
    SCM,
    BUILD,
    DEPLOY
}
