package com.verizon.vdc.model.enums;

public enum StepType {
    GIT,
    MAVEN,
    GRADLE,
    CLOUD_FOUNDRY
}
