package com.verizon.vdc.model.minors;


public class Step {

    private String name;
    private Long startedAt;
    private Long finishedAt;
    private Status status;

    public Step() {
    }

    public Step(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(Long startedAt) {
        this.startedAt = startedAt;
    }

    public Long getFinishedAt() {
        return finishedAt;
    }

    public void setFinishedAt(Long finishedAt) {
        this.finishedAt = finishedAt;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object obj) {
        Step step = (Step) obj;
        return this.name.equalsIgnoreCase(step.getName());
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
