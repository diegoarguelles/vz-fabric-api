package com.verizon.vdc.model.minors;

public class CreationStatus {

    private String percentage;
    private String status;

    public CreationStatus() {
    }

    public CreationStatus(String percentage, String status) {
        this.percentage = percentage;
        this.status = status;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
