package com.verizon.vdc.model.minors;

public enum Platform {
    JAVA,
    NODE,
    RUBY,
    PYTHON,
    LEGACY
}
