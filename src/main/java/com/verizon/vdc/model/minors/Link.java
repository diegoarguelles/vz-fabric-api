package com.verizon.vdc.model.minors;

public class Link {

    public String key;
    public String uri;

    public Link() {
    }

    public Link(String key, String uri) {
        this.key = key;
        this.uri = uri;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

}
