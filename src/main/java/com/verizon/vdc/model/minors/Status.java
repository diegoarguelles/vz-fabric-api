package com.verizon.vdc.model.minors;

import com.verizon.vdc.model.enums.StepStatusType;

public class Status {

    private int percentage;
    private StepStatusType status;

    public Status(int percentage, StepStatusType status) {
        this.percentage = percentage;
        this.status = status;
    }

    public Status() {
    }

    public int getPercentage() {
        return percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }

    public StepStatusType getStatus() {
        return status;
    }

    public void setStatus(StepStatusType status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "CreationStatus{" +
                "percentage='" + percentage + '\'' +
                ", status='" + status + "}";
    }
}
