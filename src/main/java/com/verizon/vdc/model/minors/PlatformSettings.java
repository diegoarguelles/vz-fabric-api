package com.verizon.vdc.model.minors;

public class PlatformSettings {

    private String groupId;
    private String artifactId;
    private String version;
    private String jenkinsUrl;
    private String onestashUrl;
    private Credentials jenkinsCredentials;
    private Credentials onestashCredentials;

    public PlatformSettings() {
    }

    public PlatformSettings(String jenkinsUrl, String onestashUrl, Credentials jenkinsCredentials, Credentials onestashCredentials) {
        this.jenkinsUrl = jenkinsUrl;
        this.onestashUrl = onestashUrl;
        this.jenkinsCredentials = jenkinsCredentials;
        this.onestashCredentials = onestashCredentials;
    }

    public String getJenkinsUrl() {
        return jenkinsUrl;
    }

    public void setJenkinsUrl(String jenkinsUrl) {
        this.jenkinsUrl = jenkinsUrl;
    }

    public String getOnestashUrl() {
        return onestashUrl;
    }

    public void setOnestashUrl(String onestashUrl) {
        this.onestashUrl = onestashUrl;
    }

    public Credentials getJenkinsCredentials() {
        return jenkinsCredentials;
    }

    public void setJenkinsCredentials(Credentials jenkinsCredentials) {
        this.jenkinsCredentials = jenkinsCredentials;
    }

    public Credentials getOnestashCredentials() {
        return onestashCredentials;
    }

    public void setOnestashCredentials(Credentials onestashCredentials) {
        this.onestashCredentials = onestashCredentials;
    }


    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getArtifactId() {
        return artifactId;
    }

    public void setArtifactId(String artifactId) {
        this.artifactId = artifactId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "PlatformSettings{" +
                "groupId='" + groupId + '\'' +
                ", artifactId='" + artifactId + '\'' +
                ", version='" + version + '\'' +
                ", jenkinsUrl='" + jenkinsUrl + '\'' +
                ", onestashUrl='" + onestashUrl + '\'' +
                ", jenkinsCredentials=" + jenkinsCredentials +
                ", onestashCredentials=" + onestashCredentials +
                '}';
    }
}
