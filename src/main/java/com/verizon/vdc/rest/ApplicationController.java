package com.verizon.vdc.rest;

import com.verizon.vdc.model.Application;
import com.verizon.vdc.model.enums.ProjectType;
import com.verizon.vdc.model.minors.CreationStatus;
import com.verizon.vdc.model.minors.Status;
import com.verizon.vdc.service.ApplicationService;
import com.verizon.vdc.service.BuildService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import static org.springframework.http.HttpStatus.NOT_FOUND;


@RestController
@RequestMapping("/api")
public class ApplicationController {

    private static Logger LOGGER = LoggerFactory.getLogger(ApplicationController.class);

    private BuildService buildService;
    private ApplicationService applicationService;

    @Autowired
    public ApplicationController(BuildService buildService, ApplicationService applicationService) {
        this.buildService = buildService;
        this.applicationService = applicationService;
    }

    @PostMapping("application")
    public ResponseEntity<Application> saveAndBuildApplication(@RequestBody Application application) throws URISyntaxException, IOException {
        LOGGER.info("ApplicationController.saveAndBuildProject : " + application.toString());
        Application applicationSaved = applicationService.createApp(application);

        if (applicationSaved == null) {
            LOGGER.error("The application was not saved " + application.toString());
            return null;
        }

        if(application.getProjectType() == ProjectType.FABRIC)
            buildService.build(applicationSaved);
        else
            buildService.onboard(applicationSaved);

        return ResponseEntity
                .created(new URI("/api/applications/" + applicationSaved.getId()))
                .body(applicationSaved);
    }

    @PutMapping("/application/{appId}")
    public ResponseEntity<Application> updateApplicationBuildStatus(@PathVariable("appId") String appId, @RequestBody Status status) throws URISyntaxException, IOException {
        LOGGER.info("ApplicationController.updateApplicationBuildStatus : " + appId + " , " + status.toString());
        Application applicationUpdated = applicationService.updateApplicationStatus(appId, status);
        return ResponseEntity.ok(applicationUpdated);

    }

    @GetMapping("/application/{appId}")
    public ResponseEntity<Application> getApplication(@PathVariable String appId){
        LOGGER.info("ApplicationController.getApplication::"+appId);
        Application applicationFound = applicationService.findApplicationById(appId);
        if(applicationFound!=null){
            LOGGER.info("Application found >>>>>> "+applicationFound.toString());
            return ResponseEntity.ok(applicationFound);
        }
        return new ResponseEntity<>(NOT_FOUND);
    }

    @GetMapping("/application/{appId}/creation-status")
    public ResponseEntity<Status> getCreationStatusApplication(@PathVariable String appId){
        LOGGER.info("ApplicationController.getCreationStatusApplication::"+appId);
        Status currentStatus = applicationService.findApplicationById(appId).getFabricStatus();
        if(currentStatus!=null){
            LOGGER.info("Current status >>>>>> "+currentStatus.toString());
            return ResponseEntity.ok(currentStatus);
        }
        return new ResponseEntity<>(NOT_FOUND);
    }

    @GetMapping("/applications")
    public ResponseEntity<List<Application>> getAllApplications(){
        List<Application> applications = applicationService.findAll();
        if(applications!=null){
            return ResponseEntity.ok(applications);
        }
        return new ResponseEntity<>(NOT_FOUND);
    }

}
