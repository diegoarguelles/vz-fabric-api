package com.verizon.vdc.rest.payloads;


import com.verizon.vdc.model.enums.StageType;
import com.verizon.vdc.model.minors.Status;

public class ExecutionUpdatePayload {
    private StageType stage;
    private Status status;

    public StageType getStage() {
        return stage;
    }

    public void setStage(StageType stage) {
        this.stage = stage;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
