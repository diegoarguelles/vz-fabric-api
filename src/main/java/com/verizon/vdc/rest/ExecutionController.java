package com.verizon.vdc.rest;

import com.verizon.vdc.model.Application;
import com.verizon.vdc.model.Execution;
import com.verizon.vdc.model.enums.StepStatusType;
import com.verizon.vdc.model.minors.Status;
import com.verizon.vdc.rest.payloads.ExecutionUpdatePayload;
import com.verizon.vdc.service.ApplicationService;
import com.verizon.vdc.service.ExecutionService;
import com.verizon.vdc.service.PipelineService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
@RequestMapping("/api")
public class ExecutionController {

    private static Logger LOGGER = LoggerFactory.getLogger(ApplicationController.class);

    private ExecutionService executionService;
    private PipelineService pipelineService;

    @Autowired
    public ExecutionController(ExecutionService executionService, PipelineService pipelineService) {
        this.executionService = executionService;
        this.pipelineService = pipelineService;
    }

    @PostMapping("/execution")
    public ResponseEntity<Execution> createExecution(@RequestBody Map exec) throws URISyntaxException, IOException {
        if (!exec.containsKey("appId"))
            return (ResponseEntity<Execution>) ResponseEntity.status(NOT_FOUND);

        String appId = (String)exec.get("appId");
        LOGGER.info("ExecutionController.createExecution : " + exec);
        Execution execution = executionService.createExecutionEntry(appId);

        return ResponseEntity
                .created(new URI("/api/execution/" +  appId))
                .body(execution);
    }

    @PostMapping("/execute-pipeline-on/{appId}")
    public ResponseEntity createExecution(@PathVariable String appId) throws URISyntaxException, IOException {
        LOGGER.info("ExecutionController.createExecution: Run the pipeline manually");
        pipelineService.startPipeline(appId);
        return ResponseEntity.ok(null);
    }

    @GetMapping("/executions-on/{appId}")
    public ResponseEntity<List<Execution>> retrieveAllPipelineExecutionsForApplication(@PathVariable String appId){
        LOGGER.info("ExecutionController.retrieveAllPipelineExecutionsForApplication : " + appId);
        List<Execution> executions = executionService.getAllExecutionsOnApplication(appId);
        if(executions!=null){
            return ResponseEntity.ok(executions);
        }
        return new ResponseEntity<>(NOT_FOUND);
    }

    @PutMapping("/execution/{appId}")
    public ResponseEntity<Execution> updateExecutionStatus(@PathVariable("appId") String appId, @RequestBody ExecutionUpdatePayload updatePayload)
            throws URISyntaxException, IOException {
        LOGGER.info("ExecutionController.updateExecutionStatus : " + appId + ", status: " + updatePayload.toString());

        Execution executionUpdated = executionService.updateStatus(appId, updatePayload);

        return ResponseEntity.ok(executionUpdated);
    }

}
