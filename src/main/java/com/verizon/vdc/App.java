package com.verizon.vdc;

import com.verizon.vdc.model.Application;
import com.verizon.vdc.repository.ExecutionRepository;
import com.verizon.vdc.service.ExecutionService;
import com.verizon.vdc.service.PipelineService;
import com.verizon.vdc.util.AppProperties;
import com.verizon.vdc.util.BashExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executor;

@SpringBootApplication
@EnableAsync
public class App implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}

	@Bean
	public Executor asyncExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(2);
		executor.setMaxPoolSize(2);
		executor.setQueueCapacity(500);
		executor.setThreadNamePrefix("VzFabricApi-");
		executor.initialize();
		return executor;
	}

	@Autowired
	AppProperties appProperties;

	@Override
	public void run(String... args) throws Exception {

	}
}
