package com.verizon.vdc.repository;

import com.verizon.vdc.model.Application;
import com.verizon.vdc.model.Execution;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExecutionRepository extends MongoRepository<Execution, String> {

    List<Execution> findAllByAppIdOrderByStartedAtDesc(String appId);
    List<Execution> findByAppIdAndFinished(String appId, boolean finished);

}
