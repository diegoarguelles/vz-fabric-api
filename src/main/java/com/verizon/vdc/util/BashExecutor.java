package com.verizon.vdc.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@Component
@Scope("prototype")
public class BashExecutor {

    private static Logger LOGGER = LoggerFactory.getLogger(BashExecutor.class);

    public void execute(String... params)  {
        try {
            ProcessBuilder pb = new ProcessBuilder(params);
            Process p = pb.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = null;
            while ((line = reader.readLine()) != null) {
                LOGGER.info(line);
            }
        }
        catch(IOException ex) {
            ex.printStackTrace();
        }
    }

}
