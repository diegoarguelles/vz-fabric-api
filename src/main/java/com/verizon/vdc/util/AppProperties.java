package com.verizon.vdc.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppProperties {

    @Value("${vdc.paths.workspace}")
    private String workspace;

    @Value("${vdc.paths.build}")
    private String buildFolderPath;

    @Value("${vdc.paths.pipelines}")
    private String pipelinePath;

    @Value("${vdc.paths.build_script}")
    private String buildScript;

    @Value("${vdc.paths.onboard_script}")
    private String onboardScript;

    @Value("${vdc.paths.start_pipeline}")
    private String startPipelineScript;

    @Value("${vdc.vars.pipeline_template_name}")
    private String pipelineTemplateName;

    @Value("${vdc.tools.default_cf_host}")
    private String defaultCfHost;

    @Value("${vdc.tools.default_cf_user}")
    private String defaultCfUser;

    @Value("${vdc.tools.default_cf_pass}")
    private String defaultCfPassword;

    @Value("${vdc.tools.default_cf_org}")
    private String defaultCfOrg;

    @Value("${vdc.tools.default_cf_space}")
    private String defaultCfSpace;

    @Value("${vdc.tools.default_git_project}")
    private String defaultStashProject;

    @Value("${vdc.tools.default_cf_deploy}")
    private String defaultCfDeploy;

    @Value("${vdc.paths.onboard_pipeline_template}")
    private String onboardPipelineTemplate;

    public String getOnboardPipelineTemplate() {
        return workspace.concat(onboardPipelineTemplate);
    }

    public void setOnboardPipelineTemplate(String onboardPipelineTemplate) {
        this.onboardPipelineTemplate = onboardPipelineTemplate;
    }

    public String getDefaultCfDeploy() {
        return defaultCfDeploy;
    }

    public void setDefaultCfDeploy(String defaultCfDeploy) {
        this.defaultCfDeploy = defaultCfDeploy;
    }

    public String getDefaultStashProject() {
        return defaultStashProject;
    }

    public void setDefaultStashProject(String defaultStashProject) {
        this.defaultStashProject = defaultStashProject;
    }

    public String getDefaultCfHost() {
        return defaultCfHost;
    }

    public void setDefaultCfHost(String defaultCfHost) {
        this.defaultCfHost = defaultCfHost;
    }

    public String getDefaultCfUser() {
        return defaultCfUser;
    }

    public void setDefaultCfUser(String defaultCfUser) {
        this.defaultCfUser = defaultCfUser;
    }

    public String getDefaultCfPassword() {
        return defaultCfPassword;
    }

    public void setDefaultCfPassword(String defaultCfPassword) {
        this.defaultCfPassword = defaultCfPassword;
    }

    public String getDefaultCfOrg() {
        return defaultCfOrg;
    }

    public void setDefaultCfOrg(String defaultCfOrg) {
        this.defaultCfOrg = defaultCfOrg;
    }

    public String getDefaultCfSpace() {
        return defaultCfSpace;
    }

    public void setDefaultCfSpace(String defaultCfSpace) {
        this.defaultCfSpace = defaultCfSpace;
    }

    public String getWorkspace() {
        return workspace;
    }

    public void setWorkspace(String workspace) {
        this.workspace = workspace;
    }

    public String getBuildFolderPath() {
        return workspace.concat(buildFolderPath);
    }

    public void setBuildFolderPath(String buildFolderPath) {
        this.buildFolderPath = buildFolderPath;
    }

    public String getPipelinePath() {
        return workspace.concat(pipelinePath);
    }

    public void setPipelinePath(String pipelinePath) {
        this.pipelinePath = pipelinePath;
    }

    public String getBuildScript() {
        return workspace.concat(buildScript);
    }

    public void setBuildScript(String buildScript) {
        this.buildScript = buildScript;
    }

    public String getOnboardScript() {
        return workspace.concat(onboardScript);
    }

    public void setOnboardScript(String onboardScript) {
        this.onboardScript = onboardScript;
    }

    public String getStartPipelineScript() {
        return workspace.concat(startPipelineScript);
    }

    public void setStartPipelineScript(String startPipelineScript) {
        this.startPipelineScript = startPipelineScript;
    }

    public String getPipelineTemplateName() {
        return pipelineTemplateName;
    }

    public void setPipelineTemplateName(String pipelineTemplateName) {
        this.pipelineTemplateName = pipelineTemplateName;
    }
}
