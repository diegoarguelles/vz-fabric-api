#!/usr/bin/env bash

JENKINS_URI=$1
JENKINS_JOB=$2

USER=$3
PWD=$4

curl -u $USER:$PWD -X POST $JENKINS_URI$JENKINS_JOB/build
#java -jar jenkins-cli.jar -s $JENKINS_URI build $JENKINS_JOB --username $USER --password $PWD
echo "Pipeline Started"
