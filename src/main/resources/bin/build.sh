#!/usr/bin/env bash

## usage: ./shell_executor.sh 9822 results/
APP_ID=$1
PROCESS_OUTPUT_FILE=$VDC_WORKSPACE/builds/$APP_ID.process

# APP VARS
APP_GROUP=$2
APP_ARTIFACT=$3
APP_VERSION=$4


SCM_API_PATH=/rest/api/1.0/projects/
SCM_HOST=$5

SCM_USER=$6
SCM_PASS=$7
SCM_REPOSITORY=$APP_ARTIFACT
SCM_REPLACE_REPO=$8
VZ_ID=$9


JENKINS_USER=${11}
JENKINS_PWD=${12}

SCM_PROTOCOL=${13}
JKS_PROTOCOL=${14}
SCM_PROJECT=${15}

SCM_URI=$SCM_PROTOCOL://$SCM_HOST$SCM_API_PATH$SCM_PROJECT
SCM_REPOS_URI=$SCM_URI/repos

JENKINS_URI=$JKS_PROTOCOL://${10}

# !!!!!!!!!!!!!!!!!!!!  CHANGE THIS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
EXECUTIONS_API=http://localhost:9000/api/application/$APP_ID
VDC_WORKSPACE=C:/workspaces/vdc
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

CUR_DIR=$VDC_WORKSPACE

# If replace flag is true, then remove the repo
if [ $SCM_REPLACE_REPO = '1' ]
then
    EXECUTION=`curl -u $SCM_USER:$SCM_PASS -X GET -H "Content-Type:application/json"  $SCM_REPOS_URI/$SCM_REPOSITORY | grep error`
    ERRORS_COUNT=${#EXECUTION}
    if [ $ERRORS_COUNT = 0 ]
    then
        curl -u $SCM_USER:$SCM_PASS -X DELETE -H "Content-Type:application/json" \
              $SCM_REPOS_URI/$SCM_REPOSITORY
    fi
fi

# update status
curl -X PUT -H "Content-Type:application/json" \
     -d "{\"percentage\":\"10\",\"status\":\"RUNNING\"}" $EXECUTIONS_API

echo '10%'

# Create the repository
EXECUTION=`curl -u $SCM_USER:$SCM_PASS -X POST -H "Content-Type:application/json" \
     -d "{\"name\":\"$SCM_REPOSITORY\",\"scmId\":\"git\",\"forkable\":true}" \
      $SCM_REPOS_URI | grep error`

ERRORS_COUNT=${#EXECUTION}

if [ $ERRORS_COUNT = 0 ]
then

	##### Make this repository public
    curl -u $SCM_USER:$SCM_PASS -X PUT -H "Content-Type:application/json" \
     -d "{\"public\":true}" \
     $SCM_REPOS_URI/$SCM_REPOSITORY

    # update status
    curl -X PUT -H "Content-Type:application/json" \
         -d "{\"percentage\":\"20\",\"status\":\"RUNNING\"}"  $EXECUTIONS_API
    echo '20%'



    # Create Temp dir
    TMP_DIR=$VDC_WORKSPACE/code_gen/$APP_GROUP-$APP_ARTIFACT
    rm -rf $TMP_DIR
    mkdir -p $TMP_DIR

    cd $TMP_DIR

    # Create the code
    EXECUTION=`mvn archetype:generate -DgroupId=$APP_GROUP -DartifactId=$APP_ARTIFACT -Dversion=$APP_VERSION \
    -DarchetypeGroupId=com.verizon.vdc -DarchetypeArtifactId=vdc-java-backend-archetype \
    -DarchetypeVersion=0.0.1-SNAPSHOT -DinteractiveMode=false | grep FAILURE`

    ERRORS_COUNT=${#EXECUTION}

    if [ $ERRORS_COUNT = 0 ]
    then

        curl -X PUT -H "Content-Type:application/json" \
             -d "{\"percentage\":\"40\",\"status\":\"RUNNING\"}"  $EXECUTIONS_API
        echo '40%'

        # Get into generated code and push the code
        REPO_DIR=$TMP_DIR/$APP_ARTIFACT

        REPOSITORY=$SCM_PROTOCOL://$SCM_HOST/scm/$SCM_PROJECT/$SCM_REPOSITORY.git

        # Move pipeline
        mv $VDC_WORKSPACE/pipelines/$APP_ID.pipeline $REPO_DIR/JenkinsFile
        cp $VDC_WORKSPACE/common/.gitignore $REPO_DIR/.gitignore
        cp $VDC_WORKSPACE/common/manifest.yml $REPO_DIR/manifest.yml
        cp $VDC_WORKSPACE/common/pipeline_build.xml $VDC_WORKSPACE/pipelines/$APP_ID.xml

        sed -i -e "s@APP_NAME@$APP_ARTIFACT@g; s/APP_VERSION/$APP_VERSION/g" $REPO_DIR/manifest.yml
        sed -i -e "s@GIT_URL@$REPOSITORY@g" $REPO_DIR/JenkinsFile
        sed -i -e "s@GIT_URL@$REPOSITORY@g; s/VZ_ID/$VZ_ID/g" $VDC_WORKSPACE/pipelines/$APP_ID.xml

        cd $REPO_DIR
        git init
        git add --all
        git remote add origin $SCM_PROTOCOL://$SCM_USER:$SCM_PASS@$SCM_HOST/scm/$SCM_PROJECT/$SCM_REPOSITORY.git
        git config remote.origin.url $SCM_PROTOCOL://$SCM_USER:$SCM_PASS@$SCM_HOST/scm/$SCM_PROJECT/$SCM_REPOSITORY.git
        git commit -m "Initial commit: Verizon Developer Console commit."
        git push origin master

        # Remove tmp dir
        #rm -rf $TMP_DIR

        curl -X PUT -H "Content-Type:application/json" \
             -d "{\"percentage\":\"60\",\"status\":\"RUNNING\"}"  $EXECUTIONS_API
        echo '60%'

        REPOSITORY=$SCM_PROTOCOL://$SCM_HOST/scm/$SCM_PROJECT/$SCM_REPOSITORY.git

        # Create Pipeline
        java -jar $VDC_WORKSPACE/bin/jenkins-cli.jar -s $JENKINS_URI create-job VES.TMMV.$APP_ARTIFACT.CICD < $VDC_WORKSPACE/pipelines/$APP_ID.xml --username $JENKINS_USER --password $JENKINS_PWD

		curl -X PUT -H "Content-Type:application/json" \
             -d "{\"percentage\":\"100\",\"status\":\"DONE\"}"  $EXECUTIONS_API
        echo '100%'


    else
         curl -X PUT -H "Content-Type:application/json" \
              -d "{\"percentage\":\"0\",\"status\":\"FAILED\"}"  $EXECUTIONS_API
        echo "GENERATE ARCHETYPE: $EXECUTION"
    fi

else
    curl -X PUT -H "Content-Type:application/json" \
         -d "{\"percentage\":\"0\",\"status\":\"FAILED\"}"  $EXECUTIONS_API
    echo "CREATE REPO $EXECUTION"
fi
# ------------------------------------------------

echo $!