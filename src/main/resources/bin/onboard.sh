#!/usr/bin/env bash

## usage: ./shell_executor.sh 9822 results/
APP_ID=$1

VZ_ID=$2

JENKINS_URI=http://$3
JENKINS_USER=$4
JENKINS_PWD=$5

# !!!!!!!!!!!!!!!!!!!!  CHANGE THIS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
EXECUTIONS_API=http://localhost:9000/api/application/$APP_ID
VDC_WORKSPACE=C:/workspaces/vdc
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

# deploy pipeline
cp $VDC_WORKSPACE/common/pipeline_onboard.xml $VDC_WORKSPACE/pipelines/$APP_ID.xml
sed -i -e "s/VZ_ID/$VZ_ID/g" $VDC_WORKSPACE/pipelines/$APP_ID.xml

curl -X PUT -H "Content-Type:application/json" \
             -d "{\"percentage\":\"100\",\"status\":\"DONE\"}"  $EXECUTIONS_API
echo '100%'